class CartItem < ActiveRecord::Base
  validates :product_id, presence: true
  validates :cart_id, presence: true
  validates :qty, presence: true
  
  belongs_to :product
  belongs_to :cart
end
