class Product < ActiveRecord::Base
  validates :name, presence: true
  validates :price, numericality: { only_integer: true }
  
  has_many :cart_items, source: :cart, through: :cart_items
  has_many :carts, through: :cart_items
  belongs_to :cart

end
