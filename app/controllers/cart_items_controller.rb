class CartItemsController < ApplicationController
  def create
    product = Product.find(params[:product_id])
    cart = current_cart
    cart.cart_items.create(product_id: product.id, cart_id: cart.id, qty: params[:qty])
    redirect_to cart_path(current_cart.id)
  end
  
  def destroy
    CartItem.find(params[:id]).destroy
    redirect_to cart_path(current_cart.id)
  end
  
end
