Rails.application.routes.draw do
  resources :products, only:[:index, :new, :create, :destroy]
  
  resources :cart_items, only:[:create, :destroy]
  
  resources :carts, only:[:show]
  
  root 'top#main'
  get 'top/main'
end
